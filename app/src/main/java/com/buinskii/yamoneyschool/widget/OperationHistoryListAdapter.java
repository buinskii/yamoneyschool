package com.buinskii.yamoneyschool.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.model.Operation;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by buinskii on 28.09.2015.
 */
public class OperationHistoryListAdapter extends ArrayAdapter<Operation> {

    LayoutInflater inflater;
    private List<Operation> data;
    private int resource = 0;
    private DecimalFormat decimalFormatter;
    Calendar calendar;

    public OperationHistoryListAdapter(Context context, int resource, List<Operation> objects) {
        super(context, resource, objects);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = objects;
        this.resource = resource;
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setGroupingSeparator(' ');
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatter = new DecimalFormat("###,###,##0.00", decimalFormatSymbols);
        calendar = Calendar.getInstance();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView text;
        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }
        Operation item = getItem(position);
        text = (TextView) view.findViewById(R.id.fieldDatetime);
        if (null != text) {
            DateTime dateTime = item.getDateTime();
            String useFormat = "dd.MM.yyyy kk:mm:ss";
            if (dateTime.year().get() == calendar.get(Calendar.YEAR)) {
                useFormat = "dd MMM\nkk:mm";
            }
            text.setText(new SimpleDateFormat(useFormat, Locale.getDefault()).format(new Date(dateTime.getMillis())));
        }
        text = (TextView) view.findViewById(R.id.fieldTitle);
        if (null != text) text.setText(item.getTitle());
        text = (TextView) view.findViewById(R.id.fieldAmount);
        if (null != text) {
            if (item.getDirection() == com.yandex.money.api.model.Operation.Direction.OUTGOING) {
                text.setText("- " + decimalFormatter.format(item.getAmount()));
                text.setTextColor(getContext().getResources().getColor(R.color.item_operation_outgoingColor));
            } else {
                text.setText("+ " + decimalFormatter.format(item.getAmount()));
                text.setTextColor(getContext().getResources().getColor(R.color.item_operation_incomingColor));
            }
        }

        return view;
    }

}
