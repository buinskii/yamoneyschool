package com.buinskii.yamoneyschool.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.model.RemittanceForm;
import com.buinskii.yamoneyschool.model.YandexErrorMessage;
import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.ProcessPayment;
import com.yandex.money.api.model.Error;
import com.yandex.money.api.net.OAuth2Session;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import ru.yandex.money.android.utils.ResponseReady;

public class RemittanceProceedFragment extends Fragment implements View.OnClickListener {

    private static final String PARAM_REQUEST_ID = "requestId";
    private static final String PARAM_PROTECTION_CODE = "protectionCode";
    private static final String PARAM_FORM = "form";

    private String requestId;
    private String protectionCode;
    private RemittanceForm form;
    private App app;
    private ViewGroup containerView;
    private ViewGroup containerWork;
    private View loadingView;
    private TextView fieldMessage;
    private TextView fieldPaySum;
    private TextView fieldProtectionCode;
    private TextView fieldRecipient;
    private TextView fieldRecipientSum;
    private DecimalFormat decimalFormatter;

    private OnRemittanceProceedInteractionListener interactionListener;

    public static RemittanceProceedFragment newInstance(String requestId, RemittanceForm form, String protectionCode) {
        RemittanceProceedFragment fragment = new RemittanceProceedFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_REQUEST_ID, requestId);
        args.putString(PARAM_PROTECTION_CODE, protectionCode);
        args.putParcelable(PARAM_FORM, form);
        fragment.setArguments(args);
        return fragment;
    }

    public RemittanceProceedFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            requestId = getArguments().getString(PARAM_REQUEST_ID);
            protectionCode = getArguments().getString(PARAM_PROTECTION_CODE);
            form = getArguments().getParcelable(PARAM_FORM);
        }
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setGroupingSeparator(' ');
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatter = new DecimalFormat("###,###,##0.00", decimalFormatSymbols);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        containerView = (ViewGroup) inflater.inflate(R.layout.frg_remittance_proceed, container, false);
        initViews();

        if (savedInstanceState != null) {
            initViewsStateFromBundle(savedInstanceState);
        } else {
            containerWork.setVisibility(View.INVISIBLE);
            loadingView.setVisibility(View.VISIBLE);
            startProceed();
        }

        return containerView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (App) activity.getApplication();
        try {
            interactionListener = (OnRemittanceProceedInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnRemittanceProceedInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        app = null;
        interactionListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null == outState) {
            outState = new Bundle();
        }
        provideViewsStateToBundle(outState);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.actionClose: onActionClose(); break;
            case R.id.actionRetry: onActionRetry(); break;
            case R.id.actionShare: onActionShare(); break;
        }
    }

    private void onActionClose() {
        if (null != interactionListener) {
            interactionListener.onRemittanceProceedComplete(requestId, form, protectionCode);
        }
    }

    private void onActionRetry() {
        if (null != interactionListener) {
            interactionListener.onRemittanceProceedRetry(requestId, form, protectionCode);
        }
    }

    private void onActionShare() {
        String shareBody = "";
        if (form.isProtectionCode()) {
            shareBody = String.format(
                    getString(R.string.act_remittance_share_bodyWithProtectionCode),
                    form.getRecipientSum(),
                    protectionCode,
                    form.getMessage()
            );
        } else {
            shareBody = String.format(
                    getString(R.string.act_remittance_share_body),
                    form.getRecipientSum(),
                    form.getMessage()
            );
        }
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.act_remittance_share_subject));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.act_remittance_share_title)));
    }

    private void onRequestPaymentSuccessed(ProcessPayment payment) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);

        fieldMessage.setText(getString(R.string.act_remittance_proceedResult_success));
    }

    private void onRequestPaymentFailed(Exception e) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);
        e.printStackTrace();

        String message = getString(R.string.act_remittance_proceedResult_failed);
        if (e instanceof InvalidTokenException) {
            message += ": " + getString(R.string.act_remittance_proceedResult_error_noAuth);
        } else if (e instanceof IOException) {
            message += ": " + getString(R.string.act_remittance_proceedResult_error_noInternet);
        }
        else {
            e.printStackTrace();
        }
        fieldMessage.setText(message);
    }

    private void onRequestPaymentFailed(Error error) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);

        String message = getString(R.string.act_remittance_proceedResult_failed);
        message += ": " + YandexErrorMessage.getMessage(getContext(), error);

        fieldMessage.setText(message);
    }

    private void startProceed() {
        OAuth2Session session = app.getSessionManager().getOAuth2Session();
        try {
            session.enqueue(
                    new ProcessPayment.Request(requestId).setTestResult(ProcessPayment.TestResult.SUCCESS),
                    new ResponseReady<ProcessPayment>() {
                        @Override
                        public void failure(Exception exception) {
                            onRequestPaymentFailed(exception);
                        }

                        @Override
                        protected void response(ProcessPayment requestPayment) {
                            if (null == requestPayment.error) {
                                onRequestPaymentSuccessed(requestPayment);
                            } else {
                                onRequestPaymentFailed(requestPayment.error);
                            }
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void provideViewsStateToBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = new ArrayList<>();
        state.add(fieldProtectionCode.getText().toString());
        state.add(fieldRecipientSum.getText().toString());
        state.add(fieldRecipient.getText().toString());
        state.add(fieldMessage.getText().toString());
        state.add(fieldPaySum.getText().toString());
        bundle.putStringArrayList("viewstates", state);
    }

    private void initViewsStateFromBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = bundle.getStringArrayList("viewstates");
        if (null == state || state.size() != 6) return;
        fieldProtectionCode.setText(state.get(0));
        fieldRecipientSum.setText(state.get(1));
        fieldRecipient.setText(state.get(2));
        fieldMessage.setText(state.get(4));
        fieldPaySum.setText(state.get(5));
    }

    private void initViews() {
        fieldProtectionCode = (TextView) containerView.findViewById(R.id.fieldProtectionCode);
        fieldRecipientSum = (TextView) containerView.findViewById(R.id.fieldRecipientSum);
        fieldRecipient = (TextView) containerView.findViewById(R.id.fieldRecipient);
        fieldMessage = (TextView) containerView.findViewById(R.id.fieldMessage);
        fieldPaySum = (TextView) containerView.findViewById(R.id.fieldPaySum);
        containerWork = (ViewGroup) containerView.findViewById(R.id.containerWork);
        loadingView = containerView.findViewById(R.id.loadingView);
        containerView.findViewById(R.id.actionClose).setOnClickListener(this);
        containerView.findViewById(R.id.actionShare).setOnClickListener(this);
        containerView.findViewById(R.id.actionRetry).setOnClickListener(this);

        if (form.isProtectionCode()) {
            fieldProtectionCode.setText(protectionCode);
        } else {
            fieldProtectionCode.setText(getString(R.string.act_remittance_fieldLabel_protectionCodeWithout));
        }
        fieldRecipientSum.setText(decimalFormatter.format(form.getRecipientSum()));
        fieldRecipient.setText(form.getRecipient());
        fieldMessage.setText(form.getMessage());
        fieldPaySum.setText(decimalFormatter.format(form.getPaySum()));
    }

    public interface OnRemittanceProceedInteractionListener {
        public void onRemittanceProceedComplete(String requestId, RemittanceForm form, String protectionCode);
        public void onRemittanceProceedRetry(String requestId, RemittanceForm form, String protectionCode);
    }

}
