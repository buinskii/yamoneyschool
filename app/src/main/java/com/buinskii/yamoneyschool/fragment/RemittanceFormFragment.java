package com.buinskii.yamoneyschool.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.model.RemittanceForm;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;

public class RemittanceFormFragment extends Fragment implements View.OnClickListener {

    public static final String PARAM_TEMPLATE = "template";

    private OnRemittanceFormInteractionListener interactionListener;
    private App app;
    private EditText fieldDaysCount;
    private EditText fieldMessage;
    private EditText fieldPaySum;
    private CompoundButton fieldProtectionCode;
    private EditText fieldRecipient;
    private EditText fieldRecipientSum;
    private PaymentSumProvider paymentSumProvider;
    private ViewGroup containerView;

    public static RemittanceFormFragment newInstance() {
        RemittanceFormFragment fragment = new RemittanceFormFragment();
        return fragment;
    }

    public static RemittanceFormFragment newInstance(RemittanceForm template) {
        RemittanceFormFragment fragment = new RemittanceFormFragment();
        Bundle args = new Bundle();
        args.putParcelable(PARAM_TEMPLATE, template);
        fragment.setArguments(args);
        return fragment;
    }

    public RemittanceFormFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        containerView = (ViewGroup) inflater.inflate(R.layout.frg_remittance_form, container, false);
        initViews();
        if (savedInstanceState != null) {
            initViewsStateFromBundle(savedInstanceState);
        } else if (getArguments() != null && getArguments().containsKey(PARAM_TEMPLATE)){
            initTemplate((RemittanceForm) getArguments().getParcelable(PARAM_TEMPLATE));
        }
        paymentSumProvider = new PaymentSumProvider(fieldRecipientSum, fieldPaySum);
        return containerView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (App) activity.getApplication();
        try {
            interactionListener = (OnRemittanceFormInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        app = null;
        interactionListener = null;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.actionSend) {
            sendRemittanceForm();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null == outState) {
            outState = new Bundle();
        }
        provideViewsStateToBundle(outState);
    }

    private void initTemplate(RemittanceForm template) {
        fieldProtectionCode.setChecked(template.isProtectionCode());
        fieldRecipientSum.setText(String.valueOf(template.getRecipientSum()));
        fieldRecipient.setText(template.getRecipient());
        fieldDaysCount.setText(String.valueOf(template.getDaysCount()));
        fieldMessage.setText(template.getMessage());
        fieldPaySum.setText(String.valueOf(template.getPaySum()));
    }

    private void sendRemittanceForm() {
        RemittanceForm form = new RemittanceForm();
        form.setRecipient(fieldRecipient.getText().toString().trim());
        try {
            form.setRecipientSum(Double.parseDouble(fieldRecipientSum.getText().toString().replace(" ", "")));
        } catch (NumberFormatException e) {}
        try {
            form.setPaySum(Double.parseDouble(fieldPaySum.getText().toString().replace(" ", "")));
        } catch (NumberFormatException e) {}
        form.setMessage(fieldMessage.getText().toString());
        form.setProtectionCode(fieldProtectionCode.isChecked());
        try {
            form.setDaysCount(Integer.parseInt(fieldDaysCount.getText().toString()));
        } catch (NumberFormatException e) {}

        boolean valid = true;
        if (!form.isValidRecipient()) {
            fieldRecipient.setError(getString(R.string.act_remittance_fieldError_recipient));
            valid = false;
        }
        if (!form.isValidPaySum()) {
            fieldPaySum.setError(getString(R.string.act_remittance_fieldError_paySum));
            valid = false;
        }
        if (!form.isValidRecipientSum()) {
            fieldRecipientSum.setError(getString(R.string.act_remittance_fieldError_recipientSum));
            valid = false;
        }
        if (!form.isValidDaysCount()) {
            fieldDaysCount.setError(getString(R.string.act_remittance_fieldError_daysCount));
            valid = false;
        }

        if (valid && null != interactionListener) {
            interactionListener.onRemittanceFormSendClick(form);
        }
    }

    private void provideViewsStateToBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = new ArrayList<>();
        state.add(fieldProtectionCode.isChecked() ? "1" : "0");
        state.add(fieldRecipientSum.getText().toString());
        state.add(fieldRecipient.getText().toString());
        state.add(fieldDaysCount.getText().toString());
        state.add(fieldMessage.getText().toString());
        state.add(fieldPaySum.getText().toString());
        bundle.putStringArrayList("viewstates", state);
    }

    private void initViewsStateFromBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = bundle.getStringArrayList("viewstates");
        if (null == state || state.size() != 6) return;
        fieldProtectionCode.setChecked(state.get(0).equals("1"));
        fieldRecipientSum.setText(state.get(1));
        fieldRecipient.setText(state.get(2));
        fieldDaysCount.setText(state.get(3));
        fieldMessage.setText(state.get(4));
        fieldPaySum.setText(state.get(5));
    }

    private void initViews() {
        fieldProtectionCode = (CompoundButton) containerView.findViewById(R.id.fieldProtectionCode);
        fieldRecipientSum = (EditText) containerView.findViewById(R.id.fieldRecipientSum);
        fieldRecipient = (EditText) containerView.findViewById(R.id.fieldRecipient);
        fieldDaysCount = (EditText) containerView.findViewById(R.id.fieldDaysCount);
        fieldMessage = (EditText) containerView.findViewById(R.id.fieldMessage);
        fieldPaySum = (EditText) containerView.findViewById(R.id.fieldPaySum);
        containerView.findViewById(R.id.actionSend).setOnClickListener(this);
    }

    public interface OnRemittanceFormInteractionListener {
        public void onRemittanceFormSendClick(RemittanceForm form);
    }

    private class PaymentSumProvider implements View.OnFocusChangeListener{

        private EditText sum;
        private EditText totalSum;
        private TextWatcher sumTextWatcher;
        private TextWatcher totalSumTextWatcher;
        private DecimalFormat formatter;

        public PaymentSumProvider(final EditText sum, EditText totalSum) {
            this.sum = sum;
            this.totalSum = totalSum;

            DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
            decimalFormatSymbols.setGroupingSeparator(' ');
            decimalFormatSymbols.setDecimalSeparator('.');
            formatter = new DecimalFormat("###,###,##0.00", decimalFormatSymbols);
            sumTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    onSumChanged();
                }
            };
            totalSumTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    onTotalSumChanged();
                }
            };
            sum.setOnFocusChangeListener(this);
            totalSum.setOnFocusChangeListener(this);
        }

        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                if (view == sum) {
                    sum.addTextChangedListener(sumTextWatcher);
                    totalSum.removeTextChangedListener(totalSumTextWatcher);
                    onSumChanged();
                } else if (view == totalSum) {
                    totalSum.addTextChangedListener(totalSumTextWatcher);
                    sum.removeTextChangedListener(sumTextWatcher);
                    onTotalSumChanged();
                }
            } else {
                totalSum.removeTextChangedListener(totalSumTextWatcher);
                sum.removeTextChangedListener(sumTextWatcher);

                String value = ((EditText) view).getText().toString().replace(" ", "");
                try {
                    Double num = formatter.parse(value).doubleValue();
                    ((EditText) view).setText(formatter.format(num));
                } catch (ParseException e) {}
            }
        }

        private void onSumChanged() {
            String value = sum.getText().toString().replace(" ", "");
            try {
                Double num = formatter.parse(value).doubleValue();
                num = num * 1.005;
                totalSum.setText(formatter.format(num));
            } catch (ParseException e) {
                totalSum.setText("0");
            }
        }

        private void onTotalSumChanged() {
            String value = totalSum.getText().toString().replace(" ", "");
            try {
                Double num = formatter.parse(value).doubleValue();
                num = num - num * 0.005;
                sum.setText(formatter.format(num));
            } catch (ParseException e) {
                sum.setText("0");
            }
        }
    }

}
