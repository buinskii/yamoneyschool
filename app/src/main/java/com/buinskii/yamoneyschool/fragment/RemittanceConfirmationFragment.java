package com.buinskii.yamoneyschool.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.model.RemittanceForm;
import com.buinskii.yamoneyschool.model.YandexErrorMessage;
import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.RequestPayment;
import com.yandex.money.api.net.OAuth2Session;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.yandex.money.android.utils.ResponseReady;

public class RemittanceConfirmationFragment extends Fragment implements View.OnClickListener {

    private static final String PARAM_FORM = "form";

    private RemittanceForm form;
    private App app;
    private ViewGroup containerView;
    private ViewGroup containerWork;
    private View loadingView;
    private TextView fieldDaysCount;
    private TextView fieldMessage;
    private TextView fieldPaySum;
    private TextView fieldProtectionCode;
    private TextView fieldRecipient;
    private TextView fieldRecipientSum;
    private DecimalFormat decimalFormatter;

    private String requestId;
    private String protectionCode;

    private OnRemittanceConfirmationInteractionListener interactionListener;

    public static RemittanceConfirmationFragment newInstance(RemittanceForm form) {
        RemittanceConfirmationFragment fragment = new RemittanceConfirmationFragment();
        Bundle args = new Bundle();
        args.putParcelable(PARAM_FORM, form);
        fragment.setArguments(args);
        return fragment;
    }

    public RemittanceConfirmationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            form = getArguments().getParcelable(PARAM_FORM);
        }
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setGroupingSeparator(' ');
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatter = new DecimalFormat("###,###,##0.00", decimalFormatSymbols);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        containerView = (ViewGroup) inflater.inflate(R.layout.frg_remittance_confirmation, container, false);
        initViews();

        if (savedInstanceState != null) {
            initViewsStateFromBundle(savedInstanceState);
            requestId = savedInstanceState.getString("requestId");
            protectionCode = savedInstanceState.getString("protectionCode");
            if (form.isProtectionCode()) {
                fieldProtectionCode.setText(protectionCode);
            } else {
                fieldProtectionCode.setText(getString(R.string.act_remittance_fieldLabel_protectionCodeWithout));
            }
            if (null == requestId || requestId.isEmpty()) {
                containerWork.setVisibility(View.INVISIBLE);
                loadingView.setVisibility(View.VISIBLE);
                startProcess();
            }
        }
        else {
            containerWork.setVisibility(View.INVISIBLE);
            loadingView.setVisibility(View.VISIBLE);
            startProcess();
        }

        return containerView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (App) activity.getApplication();
        try {
            interactionListener = (OnRemittanceConfirmationInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnRemittanceConfirmationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        app = null;
        interactionListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null == outState) {
            outState = new Bundle();
        }
        provideViewsStateToBundle(outState);
        outState.putString("protectionCode", protectionCode);
        outState.putString("requestId", requestId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.actionAccept: onRemittanceAccept(); break;
            case R.id.actionDiscard: onRemittanceDiscard(); break;
        }
    }

    private void startProcess() {
        OAuth2Session session = app.getSessionManager().getOAuth2Session();
        try {
            session.enqueue(
                    RequestPayment.Request.newInstance("p2p", buildFormPaymentParams(form)).setTestResult(RequestPayment.TestResult.SUCCESS),
                    new ResponseReady<RequestPayment>() {
                        @Override
                        public void failure(Exception exception) {
                            onRequestPaymentFailed(exception);
                        }

                        @Override
                        protected void response(RequestPayment requestPayment) {
                            if (null == requestPayment.error) {
                                onRequestPaymentSuccessed(requestPayment);
                            } else {
                                onRequestPaymentFailed(requestPayment.error);
                            }
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onRequestPaymentSuccessed(RequestPayment requestPayment) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);

        protectionCode = requestPayment.protectionCode;
        requestId = requestPayment.requestId;
        if (form.isProtectionCode()) {
            fieldProtectionCode.setText(protectionCode);
        } else {
            fieldProtectionCode.setText(getString(R.string.act_remittance_fieldLabel_protectionCodeWithout));
        }
    }

    private void onRequestPaymentFailed(Exception e) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);
        e.printStackTrace();

        String message = getString(R.string.act_remittance_confirmationResult_failed);
        if (e instanceof InvalidTokenException) {
            message += ": " + getString(R.string.act_remittance_proceedResult_error_noAuth);
        } else if (e instanceof IOException) {
            message += ": " + getString(R.string.act_remittance_proceedResult_error_noInternet);
        }
        else {
            e.printStackTrace();
        }
        showErrorDialog(message);
    }

    private void onRequestPaymentFailed(com.yandex.money.api.model.Error error) {
        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);

        String message = getString(R.string.act_remittance_confirmationResult_failed);
        message += ": " + YandexErrorMessage.getMessage(getContext(), error);
        showErrorDialog(message);
    }

    private void showErrorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message);
        builder.setPositiveButton(R.string.act_remittance_confirmationRetry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                containerWork.setVisibility(View.INVISIBLE);
                loadingView.setVisibility(View.VISIBLE);
                startProcess();
            }
        });
        builder.setNegativeButton(R.string.act_remittance_confirmationEnd, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (null != interactionListener) {
                    interactionListener.onRemittanceConfirmationDiscard(form);
                }
            }
        });
        builder.create().show();
    }

    private Map<String, String> buildFormPaymentParams(RemittanceForm form) {
        HashMap<String, String> paymentParameters = new HashMap<>();
        paymentParameters.put("to", form.getRecipient());
        paymentParameters.put("amount_due", String.valueOf(form.getRecipientSum()));
        paymentParameters.put("comment", String.format(getString(R.string.act_remittance_paymentComment), form.getRecipient()));
        paymentParameters.put("message", form.getMessage());
        paymentParameters.put("codepro", String.valueOf(form.isProtectionCode()));
        paymentParameters.put("expire_period", String.valueOf(form.getDaysCount()));

        return paymentParameters;
    }

    private void onRemittanceAccept() {
        if (null != interactionListener) {
            interactionListener.onRemittanceConfirmationAccept(requestId, form, protectionCode);
        }
    }

    private void onRemittanceDiscard() {
        if (null != interactionListener) {
            interactionListener.onRemittanceConfirmationDiscard(form);
        }
    }

    private void provideViewsStateToBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = new ArrayList<>();
        state.add(fieldProtectionCode.getText().toString());
        state.add(fieldRecipientSum.getText().toString());
        state.add(fieldRecipient.getText().toString());
        state.add(fieldDaysCount.getText().toString());
        state.add(fieldMessage.getText().toString());
        state.add(fieldPaySum.getText().toString());
        bundle.putStringArrayList("viewstates", state);
    }

    private void initViewsStateFromBundle(Bundle bundle) {
        if (null == bundle) return;
        ArrayList<String> state = bundle.getStringArrayList("viewstates");
        if (null == state || state.size() != 6) return;
        fieldProtectionCode.setText(state.get(0));
        fieldRecipientSum.setText(state.get(1));
        fieldRecipient.setText(state.get(2));
        fieldDaysCount.setText(state.get(3));
        fieldMessage.setText(state.get(4));
        fieldPaySum.setText(state.get(5));
    }

    private void initViews() {
        fieldProtectionCode = (TextView) containerView.findViewById(R.id.fieldProtectionCode);
        fieldRecipientSum = (TextView) containerView.findViewById(R.id.fieldRecipientSum);
        fieldRecipient = (TextView) containerView.findViewById(R.id.fieldRecipient);
        fieldDaysCount = (TextView) containerView.findViewById(R.id.fieldDaysCount);
        fieldMessage = (TextView) containerView.findViewById(R.id.fieldMessage);
        fieldPaySum = (TextView) containerView.findViewById(R.id.fieldPaySum);
        containerView.findViewById(R.id.actionAccept).setOnClickListener(this);
        containerView.findViewById(R.id.actionDiscard).setOnClickListener(this);
        containerWork = (ViewGroup) containerView.findViewById(R.id.containerWork);
        loadingView = containerView.findViewById(R.id.loadingView);

        fieldProtectionCode.setText("");
        fieldRecipientSum.setText(decimalFormatter.format(form.getRecipientSum()));
        fieldRecipient.setText(form.getRecipient());
        fieldDaysCount.setText(String.valueOf(form.getDaysCount()));
        fieldMessage.setText(form.getMessage());
        fieldPaySum.setText(decimalFormatter.format(form.getPaySum()));
    }

    public interface OnRemittanceConfirmationInteractionListener {
        public void onRemittanceConfirmationAccept(String requestId, RemittanceForm form, String protectionCode);

        public void onRemittanceConfirmationDiscard(RemittanceForm form);
    }

}
