package com.buinskii.yamoneyschool.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.yandex.money.api.methods.Token;
import com.yandex.money.api.net.OAuth2Session;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.yandex.money.android.utils.ResponseReady;

public class AuthActivity extends AppCompatActivity {

    public static final int AUTH_RESULT_CODE_SUCCESS = 1;
    public static final int AUTH_RESULT_CODE_FAIL = 2;
    public static final String AUTH_RESULT_TOKEN = "token";

    private WebView webView;
    private ProgressBar loadingProgress;
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_auth);
        app = (App) getApplication();

        webView = (WebView) findViewById(R.id.webView);
        loadingProgress = (ProgressBar) findViewById(R.id.loadingProgress);

        initWebView();
        startAuth();
    }

    private void initWebView() {
        webView.setWebViewClient(new AuthActivity.Client());
        webView.setWebChromeClient(new AuthActivity.Chrome());
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private void startAuth() {
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("client_id", app.getEnvConfiguration().getYaMoneyClientId());
        postParams.put("response_type", "code");
        postParams.put("redirect_uri", app.getEnvConfiguration().getYaMoneyRedirectUri());
        postParams.put("scope", "account-info operation-history operation-details incoming-transfers payment-p2p");
//        postParams.put("scope", "account-info operation-history operation-details incoming-transfers payment payment-shop payment-p2p money-source");
        postParams.put("instance_name", "com.buinskii.yamoneyschool");
        webView.postUrl("https://m.money.yandex.ru/oauth/authorize", this.buildPostData(postParams));
    }

    private void showProgress() {
        loadingProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        loadingProgress.setVisibility(View.INVISIBLE);
    }

    private byte[] buildPostData(Map<String, String> postParams) {
        String url = "";
        Map.Entry entry;
        for (Iterator i$ = postParams.entrySet().iterator(); i$.hasNext(); url = url + (String) entry.getKey() + "=" + this.safeUrlEncoding((String) entry.getValue()) + "&") {
            entry = (Map.Entry) i$.next();
        }
        try {
            return url.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String safeUrlEncoding(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return value;
        }
    }

    private String extractCode(String urlWithCode) {
        Uri uri = Uri.parse(urlWithCode);
        return uri.getQueryParameter("code");
    }

    private class Chrome extends WebChromeClient {

        private Chrome() {

        }

        public void onProgressChanged(WebView view, int newProgress) {
            Log.d("WebChromeClient", "progress = " + newProgress);
            if (newProgress == 100) {
                hideProgress();
            } else {
                showProgress();
            }
        }
    }

    private class Client extends WebViewClient {
        private Client() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("WebViewClient", "loading " + url);
            if (url.contains(app.getEnvConfiguration().getYaMoneyRedirectUri())) {
                OAuth2Session session = app.getSessionManager().getOAuth2Session();
                try {
                    session.enqueue(
                            new Token.Request(extractCode(url), app.getEnvConfiguration().getYaMoneyClientId(), app.getEnvConfiguration().getYaMoneyRedirectUri(), app.getEnvConfiguration().getYaMoneyClientSecret()),
                            new ResponseReady<Token>() {
                                public void failure(Exception exception) {
                                    exception.printStackTrace();
                                    Intent resultIntent = new Intent();
                                    setResult(AUTH_RESULT_CODE_FAIL, resultIntent);
                                    finish();
                                }

                                public void response(Token response) {
                                    Intent resultIntent = new Intent();
                                    resultIntent.putExtra(AUTH_RESULT_TOKEN, response.accessToken);
                                    if (null != response.error) {
                                        setResult(AUTH_RESULT_CODE_FAIL, resultIntent);
                                    } else {
                                        setResult(AUTH_RESULT_CODE_SUCCESS, resultIntent);
                                    }
                                    finish();
                                }
                            }
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return true;
            } else {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }
    }
}
