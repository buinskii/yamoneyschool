package com.buinskii.yamoneyschool.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.fragment.RemittanceConfirmationFragment;
import com.buinskii.yamoneyschool.fragment.RemittanceFormFragment;
import com.buinskii.yamoneyschool.fragment.RemittanceProceedFragment;
import com.buinskii.yamoneyschool.model.RemittanceForm;

public class RemittanceActivity extends AppCompatActivity implements
        RemittanceFormFragment.OnRemittanceFormInteractionListener,
        RemittanceConfirmationFragment.OnRemittanceConfirmationInteractionListener,
        RemittanceProceedFragment.OnRemittanceProceedInteractionListener {

    private App app;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_remittance);
        app = (App) getApplication();
        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.containerFragments, RemittanceFormFragment.newInstance())
                    .commit();
        }

    }

    @Override
    public void onRemittanceFormSendClick(RemittanceForm form) {
        fragmentManager.beginTransaction()
                .replace(R.id.containerFragments, RemittanceConfirmationFragment.newInstance(form))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRemittanceConfirmationAccept(String requestId, RemittanceForm form, String protectionCode) {
        fragmentManager.beginTransaction()
                .replace(R.id.containerFragments, RemittanceProceedFragment.newInstance(requestId, form, protectionCode))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRemittanceConfirmationDiscard(RemittanceForm form) {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void onRemittanceProceedComplete(String requestId, RemittanceForm form, String protectionCode) {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void onRemittanceProceedRetry(String requestId, RemittanceForm form, String protectionCode) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(R.id.containerFragments, RemittanceFormFragment.newInstance(form))
                .commit();
    }
}
