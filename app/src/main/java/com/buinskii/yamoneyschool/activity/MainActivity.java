package com.buinskii.yamoneyschool.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.R;
import com.buinskii.yamoneyschool.background.api.operation.HistoryTask;
import com.buinskii.yamoneyschool.background.api.operation.HistoryTaskResult;
import com.buinskii.yamoneyschool.background.task.TaskResultListenerInterface;
import com.buinskii.yamoneyschool.model.Operation;
import com.buinskii.yamoneyschool.widget.OperationHistoryListAdapter;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.AccountInfo;
import com.yandex.money.api.net.OAuth2Session;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;

import ru.yandex.money.android.utils.ResponseReady;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_AUTH = 1;

    private App app;
    private ProgressBar loadingView;
    private ProgressBar loadingHistoryView;
    private View containerWork;
    private View containerAuth;
    private View containerUserbar;
    private View containerHistory;
    private ListView historyListView;
    private TextView fieldUserBalance;
    private ImageView fieldUserAvatar;
    private TextView fieldUserAccount;
    private DrawerLayout drawerLayout;
    private ListView menuListView;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        app = (App) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initMenu();
        findViewById(R.id.actionAuthButton).setOnClickListener(this);
        loadingView = (ProgressBar) findViewById(R.id.loadingView);
        containerWork = findViewById(R.id.containerWork);
        containerAuth = findViewById(R.id.containerAuth);
        containerUserbar = findViewById(R.id.containerUserbar);
        containerHistory = findViewById(R.id.containerHistory);
        loadingHistoryView = (ProgressBar) containerHistory.findViewById(R.id.loadingHistoryView);
        historyListView = (ListView) containerHistory.findViewById(R.id.historyListView);
        fieldUserBalance = (TextView) containerUserbar.findViewById(R.id.fieldAccountBalance);
        fieldUserAvatar = (ImageView) containerUserbar.findViewById(R.id.fieldUserAvatar);
        fieldUserAccount = (TextView) containerUserbar.findViewById(R.id.fieldAccountNumber);

        String token = app.getSessionManager().getToken();
        if (token == null || token.isEmpty()) {
            loadingView.setVisibility(View.INVISIBLE);
            loadMenuForGuest();
        } else {
            onAuthorized();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawerLayout.isDrawerOpen(menuListView);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.actionAuthButton: onAuthClick(); break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AUTH) {
            if (resultCode == AuthActivity.AUTH_RESULT_CODE_SUCCESS && null != data && data.hasExtra(AuthActivity.AUTH_RESULT_TOKEN)) {
                String token = data.getStringExtra(AuthActivity.AUTH_RESULT_TOKEN);
                app.getSessionManager().setToken(token);
                onAuthorized();
            } else {

            }
        }
    }

    private void initMenu() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuListView = (ListView) findViewById(R.id.menuListView);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.act_main_openDrawer, R.string.act_main_closeDrawer) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    private void loadMenuForAuth() {
        ArrayList<String> menu = new ArrayList<>();
        menu.add(getString(R.string.act_main_menu_remittance));
        menu.add(getString(R.string.act_main_menu_logout));
        ArrayAdapter<String> menuAdapter = new ArrayAdapter<String>(this, R.layout.item_menu, menu);
        menuListView.setAdapter(menuAdapter);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) onRemittanceClick();
                if (position == 1) onLogoutClick();
                menuListView.setItemChecked(position, true);
                drawerLayout.closeDrawer(menuListView);
            }
        });
    }

    private void loadMenuForGuest() {
        ArrayList<String> menu = new ArrayList<>();
        menu.add(getString(R.string.act_main_menu_login));
        ArrayAdapter<String> menuAdapter = new ArrayAdapter<String>(this, R.layout.item_menu, menu);
        menuListView.setAdapter(menuAdapter);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                onAuthClick();
                menuListView.setItemChecked(position, true);
                drawerLayout.closeDrawer(menuListView);
            }
        });
    }

    private void onAuthClick() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivityForResult(intent, REQUEST_AUTH);
    }

    private void onRemittanceClick() {
        startActivity(new Intent(this, RemittanceActivity.class));
    }

    private void onLogoutClick() {
        onUnauthorized();
    }

    private void onUnauthorized() {
        app.getSessionManager().setToken(null);
        loadingView.setVisibility(View.INVISIBLE);
        containerAuth.setVisibility(View.VISIBLE);
        containerUserbar.setVisibility(View.INVISIBLE);
        containerHistory.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);
        loadMenuForGuest();
    }

    private void onAuthorizedWithoutInternet() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        currencyFormat.setCurrency(Currency.getInstance("RUB"));
        fieldUserAccount.setText(app.getSessionManager().getAccount() == null ? "" : app.getSessionManager().getAccount());
        fieldUserBalance.setText(currencyFormat.format(app.getSessionManager().getBalance() == null ? 0 : app.getSessionManager().getBalance()));
        if (null != app.getSessionManager().getAvatar()) {
            UrlImageViewHelper.setUrlDrawable(fieldUserAvatar, app.getSessionManager().getAvatar());
        } else {
            fieldUserAvatar.setImageResource(R.drawable.ic_guest);
        }

        loadingView.setVisibility(View.INVISIBLE);
        containerWork.setVisibility(View.VISIBLE);
        Log.d(getClass().getName(), "Отсутсвует интернет соединение");
        loadHistory();
    }

    private void onAuthorized() {
        loadMenuForAuth();
        containerWork.setVisibility(View.INVISIBLE);
        loadingView.setVisibility(View.VISIBLE);
        containerAuth.setVisibility(View.INVISIBLE);
        containerUserbar.setVisibility(View.VISIBLE);
        OAuth2Session session = app.getSessionManager().getOAuth2Session();
        try {
            session.enqueue(
                    new AccountInfo.Request(),
                    new ResponseReady<AccountInfo>() {
                        public void failure(Exception e) {
                            if (e instanceof InvalidTokenException) {
                                onUnauthorized();
                            } else if (e instanceof IOException) {
                                onAuthorizedWithoutInternet();
                            }
                            else {
                                e.printStackTrace();
                            }
                        }

                        public void response(AccountInfo response) {
                            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
                            currencyFormat.setCurrency(Currency.getInstance(response.currency.alphaCode));
                            fieldUserAccount.setText(response.account);
                            fieldUserBalance.setText(currencyFormat.format(response.balance));
                            if (null != response.avatar && null != response.avatar.url && !response.avatar.url.isEmpty()) {
                                UrlImageViewHelper.setUrlDrawable(fieldUserAvatar, response.avatar.url);
                                app.getSessionManager().setAvatar(response.avatar.url);
                            } else {
                                fieldUserAvatar.setImageResource(R.drawable.ic_guest);
                            }
                            app.getSessionManager()
                                    .setAccount(response.account)
                                    .setBalance(response.balance);
                            loadingView.setVisibility(View.INVISIBLE);
                            containerWork.setVisibility(View.VISIBLE);
                            loadHistory();
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadHistory() {
        historyListView.setVisibility(View.INVISIBLE);
        containerHistory.setVisibility(View.VISIBLE);
        loadingHistoryView.setVisibility(View.VISIBLE);
        app.getBackgroundServiceHelper()
                .getCatalogApi()
                .history()
                .onResult(new TaskResultListenerInterface<HistoryTaskResult, HistoryTask>() {
                    @Override
                    public void onTaskResult(HistoryTaskResult result, HistoryTask task) {
                        if (result.isSuccess()) {
                            onHistoryLoaded(result.getList());
                        } else {
                            onHistoryLoadFailed(result);
                        }
                    }
                })
                .execute();
    }

    private void onHistoryLoadFailed(HistoryTaskResult result) {
        historyListView.setVisibility(View.INVISIBLE);
        containerHistory.setVisibility(View.INVISIBLE);
        loadingHistoryView.setVisibility(View.INVISIBLE);
    }

    private void onHistoryLoaded(ArrayList<Operation> list) {
        OperationHistoryListAdapter listAdapter = new OperationHistoryListAdapter(this, R.layout.item_operation, list);
        historyListView.setAdapter(listAdapter);
        loadingHistoryView.setVisibility(View.INVISIBLE);
        historyListView.setVisibility(View.VISIBLE);
    }

}
