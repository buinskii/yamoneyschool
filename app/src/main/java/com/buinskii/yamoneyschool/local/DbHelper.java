package com.buinskii.yamoneyschool.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DbHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME = "combuinskiiyamoneyschool";
    public static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE operations ("
                + "id TEXT PRIMARY KEY, "
                + "status TEXT, "
                + "title TEXT, "
                + "patternId TEXT, "
                + "direction TEXT, "
                + "label TEXT, "
                + "amount REAL, "
                + "type TEXT, "
                + "dateTime TEXT"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    public void dumpTable(String tableName)
    {
        Log.d(getClass().getName(), "-- BEGIN DUMP TABLE: " + tableName + " --");
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor     = db.rawQuery(String.format("SELECT * FROM %s", tableName), null);
        dumpCursor(cursor);
        cursor.close();
        db.close();
        Log.d(getClass().getName(), "-- END DUMP TABLE: " + tableName + " --");
    }

    public void dumpCursor(Cursor cursor)
    {
        if (cursor.moveToFirst()) {
            ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>();

            Map<String, String> item;
            String[] cols           = cursor.getColumnNames();
            Integer[] colsLength       = new Integer[cols.length];
            String val;

            for (int i = 0; i < cols.length; i++) {
                colsLength[i] = (cols[i].length());
            }

            do {
                item = new HashMap<String, String>();
                for (int i = 0; i < cols.length; i++) {
                    val  = cursor.getString(i);
                    if (val == null) {
                        val = "NULL";
                    }
                    item.put(cols[i], val);
                    if (colsLength[i] < val.length()) {
                        colsLength[i] = val.length();
                    }
                }
                data.add(item);
            }
            while(cursor.moveToNext());

            if (data.size() > 0) {

                String row = "| ";
                for (int i = 0; i < cols.length; i++) {
                    row += String.format("%1$" + String.valueOf(colsLength[i]) + "s | ", cols[i]);
                }
                Log.d(getClass().getName(), String.format("%0" + String.valueOf(row.length()) + "d", 0).replace("0", "-"));
                Log.d(getClass().getName(), row);
                Log.d(getClass().getName(), String.format("%0" + String.valueOf(row.length()) + "d", 0).replace("0", "-"));


                for (int i = 0; i < data.size(); i++) {
                    item    = data.get(i);
                    row     = "| ";
                    for (int j = 0; j <cols.length; j++) {
                        row += String.format("%1$" + String.valueOf(colsLength[j]) + "s | ", item.get(cols[j]));
                    }
                    Log.d(getClass().getName(), row);
                    Log.d(getClass().getName(), String.format("%0" + String.valueOf(row.length()) + "d", 0).replace("0", "-"));
                }
            }
        }
    }

}
