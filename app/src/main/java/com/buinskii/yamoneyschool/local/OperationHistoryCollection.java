package com.buinskii.yamoneyschool.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.model.Operation;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class OperationHistoryCollection {

    public static final String TABLE = "operations";

    private static OperationHistoryCollection instance = null;
    private DbHelper dbHelper = null;

    public static OperationHistoryCollection getInstance(App application) {
        if (instance != null) return instance;
        return instance = new OperationHistoryCollection(application.getDbHelper());
    }

    private OperationHistoryCollection(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public int save(List<Operation> list) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(TABLE, "", new String[]{});
        if (null == list || list.size() == 0) {
            db.close();
            return 0;
        }

        int result = 0;
        db.beginTransaction();
        ContentValues cv;
        try {
            for (int i = 0; i < list.size(); i++) {
                Operation item = list.get(i);
                cv = new ContentValues();
                cv.put("id", item.getId());
                cv.put("status", item.getStatus().name());
                cv.put("title", item.getTitle());
                cv.put("patternId", item.getPatternId());
                cv.put("direction", item.getDirection().name());
                cv.put("label", item.getLabel());
                cv.put("amount", item.getAmount());
                cv.put("type", item.getType().name());
                cv.put("dateTime", item.getDateTime() == null ? "" : item.getDateTime().toString());
                db.insert(TABLE, null, cv);
            }
            db.setTransactionSuccessful();
        }
        catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }
        finally {
            db.endTransaction();
        }
        db.close();
        dbHelper.dumpTable(TABLE);
        return result;
    }

    public ArrayList<Operation> fetchAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Operation> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int columnId = cursor.getColumnIndex("id");
            int columnStatus = cursor.getColumnIndex("status");
            int columnTitle = cursor.getColumnIndex("title");
            int columnPatternId = cursor.getColumnIndex("patternId");
            int columnDirection = cursor.getColumnIndex("direction");
            int columnLabel = cursor.getColumnIndex("label");
            int columnAmount = cursor.getColumnIndex("amount");
            int columnType = cursor.getColumnIndex("type");
            int columnDateTime = cursor.getColumnIndex("dateTime");
            do {
                Operation item = new Operation();
                item.setId(cursor.getString(columnId));
                item.setStatus(com.yandex.money.api.model.Operation.Status.parse(cursor.getString(columnStatus)));
                item.setTitle(cursor.getString(columnTitle));
                item.setPatternId(cursor.getString(columnPatternId));
                item.setDirection(com.yandex.money.api.model.Operation.Direction.parse(cursor.getString(columnDirection)));
                item.setLabel(cursor.getString(columnLabel));
                item.setAmount(cursor.getDouble(columnAmount));
                item.setType(com.yandex.money.api.model.Operation.Type.parse(cursor.getString(columnType)));
                String dateTime = cursor.getString(columnDateTime);
                if (null != dateTime && !dateTime.isEmpty()) {
                    item.setDateTime(DateTime.parse(dateTime));
                }
                result.add(item);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return result;
    }

}
