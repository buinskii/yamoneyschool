package com.buinskii.yamoneyschool.model;

import android.util.Base64;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class DesCipher {

    private static final String cipherName      = "DESede/CFB8/NoPadding";
    private static final String keyFactoryName  = "DESede";

    private KeySpec keySpec;
    private SecretKey key;
    private IvParameterSpec iv;
    private boolean support = false;

    public DesCipher(String keyString, String ivString) {
        try {
            keySpec = new DESedeKeySpec(Base64.decode(keyString.getBytes("UTF-8"), Base64.DEFAULT));
            key     = SecretKeyFactory.getInstance(keyFactoryName).generateSecret(keySpec);
            iv      = new IvParameterSpec(Base64.decode(ivString.getBytes("UTF-8"), Base64.DEFAULT));
            support = true;
        } catch(Exception e) {
            e.printStackTrace();
            support = false;
        }
    }

    public boolean isSupport() {
        return support;
    }

    public String encrypt(String value) {
        if (!support) return value;
        try {
            Cipher ecipher = Cipher.getInstance(cipherName);
            ecipher.init(Cipher.ENCRYPT_MODE, key, iv);

            if(value == null) {
                return null;
            }

            byte[] utf8 = value.getBytes("UTF-8");
            byte[] enc  = ecipher.doFinal(utf8);

            return new String(Base64.encode(enc, Base64.DEFAULT),"UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String decrypt(String value) {
        if (!support) return value;
        try {
            Cipher dcipher = Cipher.getInstance(cipherName);
            dcipher.init(Cipher.DECRYPT_MODE, key, iv);

            if(value == null) {
                return null;
            }

            byte[] dec  = Base64.decode(value.getBytes("UTF-8"), Base64.DEFAULT);
            byte[] utf8 = dcipher.doFinal(dec);

            return new String(utf8, "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
