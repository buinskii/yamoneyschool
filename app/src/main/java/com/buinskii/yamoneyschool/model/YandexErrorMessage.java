package com.buinskii.yamoneyschool.model;

import android.content.Context;
import android.content.res.Resources;

import com.buinskii.yamoneyschool.R;
import com.yandex.money.api.model.Error;

/**
 * Created by buinskii on 27.09.2015.
 */
public class YandexErrorMessage {

    public static String getMessage(Context context, com.yandex.money.api.model.Error error) {
        Resources resources = context.getResources();
        if (error == Error.CONTRACT_NOT_FOUND) {
            return resources.getString(R.string.yandex_error_contract_not_found);
        } else if (error == Error.NOT_ENOUGH_FUNDS) {
            return resources.getString(R.string.yandex_error_not_enough_funds);
        } else if (error == Error.LIMIT_EXCEEDED) {
            return resources.getString(R.string.yandex_error_limit_exceeded);
        } else if (error == Error.MONEY_SOURCE_NOT_AVAILABLE) {
            return resources.getString(R.string.yandex_error_money_source_not_available);
        } else if (error == Error.ILLEGAL_PARAM_CSC) {
            return resources.getString(R.string.yandex_error_illegal_param_csc);
        } else if (error == Error.PAYMENT_REFUSED) {
            return resources.getString(R.string.yandex_error_payment_refused);
        } else if (error == Error.AUTHORIZATION_REJECT) {
            return resources.getString(R.string.yandex_error_authorization_reject);
        } else if (error == Error.ACCOUNT_BLOCKED) {
            return resources.getString(R.string.yandex_error_account_blocked);
        } else {
            return resources.getString(R.string.yandex_error_unknown);
        }
    }

}
