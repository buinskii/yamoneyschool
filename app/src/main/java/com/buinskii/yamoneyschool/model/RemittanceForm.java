package com.buinskii.yamoneyschool.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by buinskii on 27.09.2015.
 */
public class RemittanceForm implements Parcelable {

    private String recipient = "";
    private double recipientSum = 0d;
    private double paySum = 0d;
    private String message= "";
    private boolean protectionCode = false;
    private int daysCount = 0;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        if (null == recipient) recipient = "";
        this.recipient = recipient;
    }

    public double getRecipientSum() {
        return recipientSum;
    }

    public void setRecipientSum(double recipientSum) {
        this.recipientSum = recipientSum;
    }

    public double getPaySum() {
        return paySum;
    }

    public void setPaySum(double paySum) {
        this.paySum = paySum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if (null == message) message = "";
        this.message = message;
    }

    public boolean isProtectionCode() {
        return protectionCode;
    }

    public void setProtectionCode(boolean protectionCode) {
        this.protectionCode = protectionCode;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public boolean isValidRecipient() {
        return true;
    }

    public boolean isValidRecipientSum() {
        return recipientSum > 0d;
    }

    public boolean isValidPaySum() {
        return paySum > 0d;
    }

    public boolean isValidMessage() {
        return true;
    }

    public boolean isValidDaysCount() {
        return daysCount >= 1 && daysCount <= 365;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(recipient);
        parcel.writeDouble(recipientSum);
        parcel.writeDouble(paySum);
        parcel.writeString(message);
        parcel.writeInt(protectionCode ? 1 : 0);
        parcel.writeInt(daysCount);
    }

    public static final Parcelable.Creator<RemittanceForm> CREATOR = new Parcelable.Creator<RemittanceForm>() {

        @Override
        public RemittanceForm createFromParcel(Parcel source) {
            RemittanceForm form = new RemittanceForm();
            form.setRecipient(source.readString());
            form.setRecipientSum(source.readDouble());
            form.setPaySum(source.readDouble());
            form.setMessage(source.readString());
            form.setProtectionCode(source.readInt() == 1);
            form.setDaysCount(source.readInt());
            return form;
        }

        @Override
        public RemittanceForm[] newArray(int size) {
            return new RemittanceForm[size];
        }
    };

}
