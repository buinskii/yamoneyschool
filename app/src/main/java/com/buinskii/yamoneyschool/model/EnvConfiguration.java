package com.buinskii.yamoneyschool.model;

import android.content.Context;

import com.buinskii.yamoneyschool.BuildConfig;

/**
 * Created by buinskii on 23.09.2015.
 */
public class EnvConfiguration {

    private Context context;
    private DesCipher cipher;
    private String yamoneyClientId;
    private String yamoneyClientSecret;
    private String yamoneyRedirectUri;

    public EnvConfiguration(Context context) {
        this.context = context;
        this.cipher = new DesCipher(BuildConfig.DESCIPHERKEY, BuildConfig.DESCIPHERIV);
    }

    public String getYaMoneyClientId() {
        if (null != yamoneyClientId) return yamoneyClientId;
        yamoneyClientId = cipher.isSupport()
                ? cipher.decrypt(BuildConfig.YAMONEY_CLIENT_ID)
                : BuildConfig.YAMONEY_CLIENT_ID;
        return yamoneyClientId;
    }

    public String getYaMoneyRedirectUri() {
        if (null != yamoneyRedirectUri) return yamoneyRedirectUri;
        yamoneyRedirectUri = cipher.isSupport()
                ? cipher.decrypt(BuildConfig.YAMONEY_REDIRECT_URI)
                : BuildConfig.YAMONEY_REDIRECT_URI;
        return yamoneyRedirectUri;
    }

    public String getYaMoneyClientSecret() {
        if (null != yamoneyClientSecret) return yamoneyClientSecret;
        yamoneyClientSecret = cipher.isSupport()
                ? cipher.decrypt(BuildConfig.YAMONEY_CLIENT_SECRET)
                : BuildConfig.YAMONEY_CLIENT_SECRET;
        return yamoneyClientSecret;
    }

}
