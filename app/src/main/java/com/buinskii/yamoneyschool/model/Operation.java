package com.buinskii.yamoneyschool.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by buinskii on 27.09.2015.
 */
public class Operation implements Parcelable {

    private String id = "";
    private com.yandex.money.api.model.Operation.Status status;
    private String title = "";
    private String patternId = "";
    private com.yandex.money.api.model.Operation.Direction direction;
    private String label = "";
    private Double amount = null;
    private com.yandex.money.api.model.Operation.Type type;
    private DateTime dateTime;

    public Operation() {
    }

    public Operation(com.yandex.money.api.model.Operation operation) {
        setId(operation.operationId);
        setStatus(operation.status);
        setTitle(operation.title);
        setPatternId(operation.patternId);
        setDirection(operation.direction);
        setLabel(operation.label);
        setAmount(operation.amount.doubleValue());
        setType(operation.type);
        setDateTime(operation.datetime);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (null == id) id = "";
        this.id = id;
    }

    public com.yandex.money.api.model.Operation.Status getStatus() {
        return status;
    }

    public void setStatus(com.yandex.money.api.model.Operation.Status status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (null == title) title = "";
        this.title = title;
    }

    public String getPatternId() {
        return patternId;
    }

    public void setPatternId(String patternId) {
        if (null == patternId) patternId = "";
        this.patternId = patternId;
    }

    public com.yandex.money.api.model.Operation.Direction getDirection() {
        return direction;
    }

    public void setDirection(com.yandex.money.api.model.Operation.Direction direction) {
        this.direction = direction;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        if (null == label) label = "";
        this.label = label;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public com.yandex.money.api.model.Operation.Type getType() {
        return type;
    }

    public void setType(com.yandex.money.api.model.Operation.Type type) {
        this.type = type;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getId());
        parcel.writeSerializable(getStatus());
        parcel.writeString(getTitle());
        parcel.writeString(getPatternId());
        parcel.writeSerializable(getDirection());
        parcel.writeString(getLabel());
        parcel.writeDouble(getAmount());
        parcel.writeSerializable(getType());
        DateTime dateTime = getDateTime();
        parcel.writeString(null == dateTime ? "" : dateTime.toString());
    }

    public static final Parcelable.Creator<Operation> CREATOR = new Parcelable.Creator<Operation>() {

        @Override
        public Operation createFromParcel(Parcel source) {
            Operation operation = new Operation();
            operation.setId(source.readString());
            operation.setStatus((com.yandex.money.api.model.Operation.Status) source.readSerializable());
            operation.setTitle(source.readString());
            operation.setPatternId(source.readString());
            operation.setDirection((com.yandex.money.api.model.Operation.Direction) source.readSerializable());
            operation.setLabel(source.readString());
            operation.setAmount(source.readDouble());
            operation.setType((com.yandex.money.api.model.Operation.Type) source.readSerializable());

            String dateTime = source.readString();
            if (null != dateTime && !dateTime.isEmpty()) {
                operation.setDateTime(DateTime.parse(dateTime));
            }
            return operation;
        }

        @Override
        public Operation[] newArray(int size) {
            return new Operation[size];
        }

    };

}
