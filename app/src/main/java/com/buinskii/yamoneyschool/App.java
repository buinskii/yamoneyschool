package com.buinskii.yamoneyschool;

import android.app.Application;

import com.buinskii.yamoneyschool.background.BackgroundServiceHelper;
import com.buinskii.yamoneyschool.local.DbHelper;
import com.buinskii.yamoneyschool.model.EnvConfiguration;
import com.buinskii.yamoneyschool.session.SessionManager;

/**
 * Created by buinskii on 23.09.2015.
 */
public class App extends Application {

    private SessionManager sessionManager;
    private EnvConfiguration envConfiguration;
    private BackgroundServiceHelper backgroundServiceHelper;
    private DbHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public SessionManager getSessionManager() {
        if (null == sessionManager) {
            sessionManager = new SessionManager(getApplicationContext(), getEnvConfiguration());
        }
        return sessionManager;
    }

    public EnvConfiguration getEnvConfiguration() {
        if (null == envConfiguration) {
            envConfiguration = new EnvConfiguration(getApplicationContext());
        }
        return envConfiguration;
    }

    public DbHelper getDbHelper() {
        if (null == dbHelper) dbHelper = new DbHelper(getApplicationContext());
        return dbHelper;
    }

    public BackgroundServiceHelper getBackgroundServiceHelper() {
        if (backgroundServiceHelper != null) {
            return backgroundServiceHelper;
        }

        return backgroundServiceHelper = new BackgroundServiceHelper(this);
    }
}
