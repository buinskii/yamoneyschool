package com.buinskii.yamoneyschool.background.task;

public interface TaskInterface<E extends TaskResultInterface, D extends TaskInterface> {
    public TaskInterface onResult(TaskResultListenerInterface<E, D> listener);
    public int getId();
    public int execute();
}
