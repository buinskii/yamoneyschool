package com.buinskii.yamoneyschool.background.task;


import com.buinskii.yamoneyschool.background.BackgroundService;

public abstract class AbstractTaskHandler implements TaskHandlerInterface {
    protected BackgroundService service;
    public AbstractTaskHandler(BackgroundService service) {
        this.service = service;
    }
}
