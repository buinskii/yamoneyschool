package com.buinskii.yamoneyschool.background.api.operation;

import android.content.Intent;
import android.os.Bundle;

import com.buinskii.yamoneyschool.background.BackgroundServiceHelper;
import com.buinskii.yamoneyschool.background.api.BackgroundOperationApi;
import com.buinskii.yamoneyschool.background.task.AbstractTask;
import com.buinskii.yamoneyschool.background.task.TaskInterface;
import com.buinskii.yamoneyschool.background.task.TaskResultListenerInterface;
import com.yandex.money.api.methods.OperationHistory;

import org.joda.time.DateTime;

import java.util.HashSet;

public class HistoryTask extends AbstractTask {

    private TaskResultListenerInterface<HistoryTaskResult, HistoryTask> resultListener;
    private String startRecord = "";
    private DateTime from = null;
    private DateTime till = null;
    private int records = 20;
    private HashSet<OperationHistory.FilterType> types = null;

    public HistoryTask(BackgroundServiceHelper backgroundServiceHelper) {
        super(backgroundServiceHelper);
    }

    @Override
    protected void onHandlerReceiveResult(int resultCode, Bundle resultData) {
        if (resultListener != null) {
            resultListener.onTaskResult(new HistoryTaskResult(resultCode, resultData), this);
        }
    }

    @Override
    protected Intent factoryServiceIntent() {
        Intent intent = super.factoryServiceIntent();
        intent.setAction(BackgroundOperationApi.ACTION_HISTORY);
        if (null != from) intent.putExtra(HistoryTaskHandler.PARAM_FROM, from);
        if (null != till) intent.putExtra(HistoryTaskHandler.PARAM_TILL, till);
        if (null != startRecord && !startRecord.isEmpty()) intent.putExtra(HistoryTaskHandler.PARAM_START_RECORD, startRecord);
        if (null != types && !types.isEmpty()) intent.putExtra(HistoryTaskHandler.PARAM_TYPE, types);
        intent.putExtra(HistoryTaskHandler.PARAM_RECORDS, records);
        return intent;
    }

    @Override
    public TaskInterface onResult(TaskResultListenerInterface listener) {
        resultListener = listener;
        return this;
    }

    public String getStartRecord() {
        return startRecord;
    }

    public HistoryTask setStartRecord(String startRecord) {
        if (null == startRecord) startRecord = "";
        this.startRecord = startRecord;
        return this;
    }

    public DateTime getFrom() {
        return from;
    }

    public HistoryTask setFrom(DateTime from) {
        this.from = from;
        return this;
    }

    public DateTime getTill() {
        return till;
    }

    public HistoryTask setTill(DateTime till) {
        this.till = till;
        return this;
    }

    public int getRecords() {
        return records;
    }

    public HistoryTask setRecords(int records) {
        this.records = records;
        return this;
    }

    public HashSet<OperationHistory.FilterType> getTypes() {
        return types;
    }

    public HistoryTask setTypes(HashSet<OperationHistory.FilterType> types) {
        this.types = types;
        return this;
    }

    public HistoryTask addType(OperationHistory.FilterType type) {
        if (null == types) types = new HashSet<>();
        types.add(type);
        return this;
    }
}
