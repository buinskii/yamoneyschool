package com.buinskii.yamoneyschool.background;

import android.app.Application;
import android.content.Intent;

import com.buinskii.yamoneyschool.background.api.BackgroundOperationApi;

import java.util.concurrent.atomic.AtomicInteger;

public class BackgroundServiceHelper {
    private Application application;
    private AtomicInteger idCounter = new AtomicInteger();
    private BackgroundOperationApi catalogApi;

    public BackgroundServiceHelper(Application application) {
        this.application = application;
    }

    public int execute(Intent intent) {
        int id = idCounter.getAndIncrement();
        intent.setClass(application.getApplicationContext(), BackgroundService.class);
        intent.putExtra(BackgroundService.INTENT_TASK_ID, id);
        application.startService(intent);
        return id;
    }

    public BackgroundOperationApi getCatalogApi() {
        if (null == catalogApi) catalogApi = new BackgroundOperationApi(this);
        return catalogApi;
    }

    public Application getApplication() {
        return application;
    }
}
