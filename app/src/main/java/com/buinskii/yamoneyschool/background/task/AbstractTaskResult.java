package com.buinskii.yamoneyschool.background.task;

import android.os.Bundle;

import com.buinskii.yamoneyschool.background.BackgroundService;

public abstract class AbstractTaskResult implements TaskResultInterface {

    protected int resultCode = 0;
    protected Bundle resultData = null;

    public AbstractTaskResult(int resultCode, Bundle resultData) {
        this.resultCode = resultCode;
        if (null == resultData) {
            resultData = new Bundle();
        }
        this.resultData = resultData;
    }

    @Override
    public boolean isSuccess() {
        return getErrorCode() == 0;
    }

    @Override
    public boolean isServerUnavailable() {
        return getErrorCode() == BackgroundService.ERROR_SERVER_UNAVAILABLE;
    }

    @Override
    public int getErrorCode() {
        return resultCode;
    }

    @Override
    public String getErrorMessage() {
        String result = resultData.getString(BackgroundService.INTENT_ERROR_MESSAGE);
        if (null == result) result = "Unknown error";
        return result;
    }

}