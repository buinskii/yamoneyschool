package com.buinskii.yamoneyschool.background.task;

public interface TaskResultInterface {
    public boolean isSuccess();
    public int getErrorCode();
    public boolean isServerUnavailable();
    public String getErrorMessage();
}
