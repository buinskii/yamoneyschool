package com.buinskii.yamoneyschool.background.task;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.buinskii.yamoneyschool.background.BackgroundService;
import com.buinskii.yamoneyschool.background.BackgroundServiceHelper;

public abstract class AbstractTask<E extends TaskResultInterface, D extends TaskInterface> implements TaskInterface<E, D> {
    protected int id = 0;
    protected BackgroundServiceHelper backgroundServiceHelper;

    public AbstractTask(BackgroundServiceHelper backgroundServiceHelper) {
        this.backgroundServiceHelper = backgroundServiceHelper;
    }

    @Override
    public int execute() {
        Intent intent = factoryServiceIntent();
        intent.putExtra(BackgroundService.INTENT_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                onHandlerReceiveResult(resultCode, resultData);
            }
        });
        id = backgroundServiceHelper.execute(intent);
        return id;
    }

    public int getId() {
        return id;
    }

    protected abstract void onHandlerReceiveResult(int resultCode, Bundle resultData);

    protected Intent factoryServiceIntent() {
        return new Intent();
    }

}
