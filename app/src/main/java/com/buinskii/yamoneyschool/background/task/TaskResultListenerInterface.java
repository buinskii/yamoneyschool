package com.buinskii.yamoneyschool.background.task;

public interface TaskResultListenerInterface<E extends TaskResultInterface, D extends TaskInterface> {
    public void onTaskResult(E result, D task);
}