package com.buinskii.yamoneyschool.background.api.operation;

import android.os.Bundle;

import com.buinskii.yamoneyschool.background.task.AbstractTaskResult;
import com.buinskii.yamoneyschool.model.Operation;

import java.util.ArrayList;

public class HistoryTaskResult extends AbstractTaskResult {

    private ArrayList<Operation> list = null;
    private String startRecord = null;

    public HistoryTaskResult(int resultCode, Bundle resultData) {
        super(resultCode, resultData);
    }

    public ArrayList<Operation> getList() {
        if (null == list) {
            try {
                list = resultData.getParcelableArrayList(HistoryTaskHandler.RESULT_LIST);
            } catch (Exception e) {
                e.printStackTrace();
                list = new ArrayList<>();
            }
        }
        return list;
    }

    public String getStartRecord() {
        if (null == startRecord) {
            startRecord = resultData.getString(HistoryTaskHandler.RESULT_START_RECORD);
            if (null == startRecord) startRecord = "";
        }
        return startRecord;
    }
}
