package com.buinskii.yamoneyschool.background.api.operation;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.buinskii.yamoneyschool.App;
import com.buinskii.yamoneyschool.background.BackgroundService;
import com.buinskii.yamoneyschool.background.task.AbstractTaskHandler;
import com.buinskii.yamoneyschool.local.OperationHistoryCollection;
import com.buinskii.yamoneyschool.model.Operation;
import com.yandex.money.api.exceptions.InsufficientScopeException;
import com.yandex.money.api.exceptions.InvalidRequestException;
import com.yandex.money.api.exceptions.InvalidTokenException;
import com.yandex.money.api.methods.OperationHistory;
import com.yandex.money.api.net.OAuth2Session;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class HistoryTaskHandler extends AbstractTaskHandler {

    public static final String RESULT_LIST = "com.buinskii.yamoneyschool.background.api.operation.result.list";
    public static final String RESULT_START_RECORD = "com.buinskii.yamoneyschool.background.api.operation.result.startRecord";
    public static final String PARAM_FROM = "com.buinskii.yamoneyschool.background.api.operation.param.from";
    public static final String PARAM_TILL = "com.buinskii.yamoneyschool.background.api.operation.param.till";
    public static final String PARAM_TYPE = "com.buinskii.yamoneyschool.background.api.operation.param.type";
    public static final String PARAM_RECORDS = "com.buinskii.yamoneyschool.background.api.operation.param.records";
    public static final String PARAM_START_RECORD = "com.buinskii.yamoneyschool.background.api.operation.param.startRecord";

    public HistoryTaskHandler(BackgroundService service) {
        super(service);
    }

    @Override
    public void handleIntent(Intent intent, ResultReceiver receiver) {
        Bundle result = new Bundle();
        DateTime from = (DateTime) intent.getSerializableExtra(PARAM_FROM);
        DateTime till = (DateTime) intent.getSerializableExtra(PARAM_TILL);
        int records = intent.getIntExtra(PARAM_RECORDS, 20);
        String startRecord = intent.getStringExtra(PARAM_START_RECORD);
        HashSet<OperationHistory.FilterType> types = (HashSet<OperationHistory.FilterType>) intent.getSerializableExtra(PARAM_TYPE);

        OperationHistoryCollection collection = OperationHistoryCollection.getInstance((App) service.getApplication());
        OperationHistory.Request.Builder builder = new OperationHistory.Request.Builder();
        if (null != from) builder.setFrom(from);
        if (null != till) builder.setTill(till);
        if (null != startRecord && !startRecord.isEmpty()) builder.setStartRecord(startRecord);
        if (null != types && types.size() > 0) builder.setTypes(types);
        OperationHistory.Request request = builder.setRecords(records)
            .createRequest();
        OAuth2Session session = ((App) service.getApplication()).getSessionManager().getOAuth2Session();
        OperationHistory requestResult = null;
        try {
            requestResult = session.execute(request);
        } catch (IOException e) {

            result.putParcelableArrayList(RESULT_LIST, collection.fetchAll());
            receiver.send(0, result);
//            receiver.send(BackgroundService.ERROR_NETWORK_UNAVAILABLE, result);
            return;
        } catch (InvalidRequestException e) {
            e.printStackTrace();
            receiver.send(BackgroundService.ERROR_RUNTIME, result);
            return;
        } catch (InvalidTokenException e) {
            e.printStackTrace();
            receiver.send(BackgroundService.ERROR_RUNTIME, result);
            return;
        } catch (InsufficientScopeException e) {
            e.printStackTrace();
            receiver.send(BackgroundService.ERROR_RUNTIME, result);
            return;
        }
        if (null != requestResult.nextRecord && !requestResult.nextRecord.isEmpty()) {
            result.putString(RESULT_START_RECORD, requestResult.nextRecord);
        }

        ArrayList<Operation> list = new ArrayList<>();
        for (int i = 0; i < requestResult.operations.size(); i++) {
            list.add(new Operation(requestResult.operations.get(i)));
        }
        collection.save(list);
        result.putParcelableArrayList(RESULT_LIST, list);
        receiver.send(0, result);
    }
}
