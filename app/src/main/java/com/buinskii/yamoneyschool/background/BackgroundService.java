package com.buinskii.yamoneyschool.background;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.buinskii.yamoneyschool.background.api.BackgroundOperationApi;
import com.buinskii.yamoneyschool.background.task.TaskHandlerInterface;

import java.util.HashMap;
import java.util.Map;

public class BackgroundService extends IntentService {

    public static final String INTENT_TASK_ID = "com.buinskii.yamoneyschool.service.background.task_id";
    public static final String INTENT_ERROR_CODE = "com.buinskii.yamoneyschool.service.background.error_code";
    public static final String INTENT_ERROR_MESSAGE = "com.buinskii.yamoneyschool.service.background.error_message";
    public static final String INTENT_RESULT_RECEIVER = "com.buinskii.yamoneyschool.service.background.result_receiver";

    public static final int ERROR_RUNTIME = 1;
    public static final int ERROR_SERVER_UNAVAILABLE = 2;
    public static final int ERROR_NETWORK_UNAVAILABLE = 3;

    private Map<String, Class> tasks = new HashMap<>();

    public BackgroundService() {
        super("BackgroundService");

        Map<String, Class> tmpList;
        tmpList = BackgroundOperationApi.getActionHandlers();
        for (String action: tmpList.keySet()) {
            if (!tasks.containsKey(action)) {
                tasks.put(action, tmpList.get(action));
            }
        }
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String action = intent.getAction();
                ResultReceiver receiver = null;
                try {
                    receiver = intent.getParcelableExtra(INTENT_RESULT_RECEIVER);
                    if (null == receiver) {
                        Log.w(getClass().getName(), String.format("The handler for action '%s' execute without result receiver", action));
                    }
                    if (!tasks.containsKey(action)) {
                        throw new RuntimeException(String.format("The are no registered handler for action '%s'", action));
                    }
                    Class handlerClass = tasks.get(action);

                    Object handler = handlerClass.getConstructor(BackgroundService.class).newInstance(BackgroundService.this);
                    if (!(handler instanceof TaskHandlerInterface)) {
                        throw new RuntimeException(String.format("The registered handler for action '%s' must implement TaskHandlerInterface", action));
                    }
                    ((TaskHandlerInterface) handler).handleIntent(intent, receiver);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    if (receiver != null) {
                        Bundle result = new Bundle();
                        result.putInt(INTENT_ERROR_CODE, ERROR_RUNTIME);
                        result.putString(INTENT_ERROR_MESSAGE, e.getMessage());
                        receiver.send(ERROR_RUNTIME, result);
                    }
                }
            }
        });
        thread.run();
    }
}
