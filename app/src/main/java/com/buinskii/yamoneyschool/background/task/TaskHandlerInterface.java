package com.buinskii.yamoneyschool.background.task;

import android.content.Intent;
import android.os.ResultReceiver;

public interface TaskHandlerInterface {
    public void handleIntent(Intent intent, ResultReceiver receiver);
}
