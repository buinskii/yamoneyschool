package com.buinskii.yamoneyschool.background.api;

import com.buinskii.yamoneyschool.background.BackgroundServiceHelper;
import com.buinskii.yamoneyschool.background.api.operation.HistoryTask;
import com.buinskii.yamoneyschool.background.api.operation.HistoryTaskHandler;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

public class BackgroundOperationApi {

    public static final String ACTION_HISTORY = "com.buinskii.yamoneyschool.service.background.api.operation.history";

    private BackgroundServiceHelper backgroundServiceHelper;

    public BackgroundOperationApi(BackgroundServiceHelper backgroundServiceHelper) {
        this.backgroundServiceHelper = backgroundServiceHelper;
    }

    public static Map<String, Class> getActionHandlers() {
        Map<String, Class> list = new HashMap<>();
        list.put(ACTION_HISTORY, HistoryTaskHandler.class);
        return list;
    }

    public HistoryTask history() {
        HistoryTask task = new HistoryTask(backgroundServiceHelper);
        task.setTill(DateTime.now());
        return task;
    }

}
