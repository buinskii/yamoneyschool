package com.buinskii.yamoneyschool.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.buinskii.yamoneyschool.BuildConfig;
import com.buinskii.yamoneyschool.model.DesCipher;
import com.buinskii.yamoneyschool.model.EnvConfiguration;
import com.yandex.money.api.net.DefaultApiClient;
import com.yandex.money.api.net.OAuth2Session;

import java.math.BigDecimal;

/**
 * Created by buinskii on 23.09.2015.
 */
public class SessionManager {

    private static final String PREFERENCE_FILE = "session";
    private static final String PREFERENCE_TOKEN = "token";
    private static final String PREFERENCE_AVATAR = "avatar";
    private static final String PREFERENCE_BALANCE = "balance";
    private static final String PREFERENCE_ACCOUNT = "account";

    private Context context;
    private EnvConfiguration envConfiguration;
    private SharedPreferences preferences;
    private OAuth2Session OAuth2Session;
    private DesCipher cipher;

    public SessionManager(Context context, EnvConfiguration envConfiguration) {
        this.context = context;
        this.envConfiguration = envConfiguration;
        this.preferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        this.cipher = new DesCipher(BuildConfig.DESCIPHERKEY, BuildConfig.DESCIPHERIV);
    }

    public String getToken() {
        String token = preferences.getString(PREFERENCE_TOKEN, null);
        if (token != null && cipher.isSupport()) token = cipher.decrypt(token);
        return token;
    }

    public SessionManager setToken(String token) {
        if (null == token || token.trim().isEmpty()) {
            preferences.edit().remove(PREFERENCE_TOKEN).commit();
        } else {
            if (cipher.isSupport()) token = cipher.encrypt(token);
            preferences.edit().putString(PREFERENCE_TOKEN, token).commit();
        }
        return this;
    }

    public String getAvatar() {
        return preferences.getString(PREFERENCE_AVATAR, null);
    }

    public SessionManager setAvatar(String avatar) {
        if (null == avatar || avatar.trim().isEmpty()) {
            preferences.edit().remove(PREFERENCE_AVATAR).commit();
        } else {
            preferences.edit().putString(PREFERENCE_AVATAR, avatar).commit();
        }
        return this;
    }

    public String getAccount() {
        return preferences.getString(PREFERENCE_ACCOUNT, null);
    }

    public SessionManager setAccount(String account) {
        if (null == account || account.trim().isEmpty()) {
            preferences.edit().remove(PREFERENCE_ACCOUNT).commit();
        } else {
            preferences.edit().putString(PREFERENCE_ACCOUNT, account).commit();
        }
        return this;
    }

    public BigDecimal getBalance() {
        if (!preferences.contains(PREFERENCE_BALANCE)) {
            return null;
        }
        return new BigDecimal(preferences.getFloat(PREFERENCE_BALANCE, 0f));
    }

    public SessionManager setBalance(BigDecimal balance) {
        if (null == balance) {
            preferences.edit().remove(PREFERENCE_AVATAR).commit();
        } else {
            preferences.edit().putFloat(PREFERENCE_AVATAR, balance.floatValue()).commit();
        }
        return this;
    }

    public OAuth2Session getOAuth2Session() {
        if (null != OAuth2Session) {
            OAuth2Session.setAccessToken(getToken());
            return OAuth2Session;
        }

        OAuth2Session = new OAuth2Session(new DefaultApiClient(envConfiguration.getYaMoneyClientId()));
        OAuth2Session.setAccessToken(getToken());
        return OAuth2Session;
    }

}
